#coding:utf-8
import os
def mem_usage():
    f = os.popen("free -m |grep Mem |awk '{print $2,$3}'")
    (all_m,use_m) =  f.readline().strip().split()
    user_age = int(float(use_m)/float(all_m)*100)
    return str(user_age)+"%"


def load_avg():
    f = os.popen("uptime | sed 's/,//g' | awk '{print $10}'")
    return f.read().strip()

def disk_usage():
    f = os.popen("df -m | grep '/$' | awk '{print $5}'")
    return f.read().strip()

def cpu_usage():
    f=os.popen("top -bi -n 1| awk '{print $2,$4}'").readlines()[2].strip().split()[0]
    return f
def get_date_time():
    f=os.popen('date').readline().strip()
    my_date=f[7:10]+'-'+f[4:6]
    my_time=f[11:19]
    return (my_date,my_time)
def get_user():
    f=os.popen('whoami').readline().strip()
    return f

def get_ip():
    f=os.popen('ifconfig | grep inet | head -3 | tail -1').read().split()[1]
    return f

    
def get_sys_info():
    testInfo={}
    testInfo['info_ip'] = get_ip()
    testInfo['info_cpu'] = cpu_usage()
    testInfo['info_memory'] = mem_usage()
    testInfo['info_load'] = load_avg()
    testInfo['info_disk'] = disk_usage()
    testInfo['info_date'] = get_date_time()[0]
    testInfo['info_time'] = get_date_time()[1]
    testInfo['info_user'] = get_user()
    return testInfo

if __name__ == "__main__":
    '''
    print("-----")
    print(mem_usage())
    print("-----")

    print(load_avg())
    print("-----")

    
    print(disk_usage())
    print("-----")

    print(cpu_usage())
    print("-----")
    
    print(get_date_time())
    print("-----")
    
    print(get_user())
    '''
    print(get_sys_info())